This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Common Security

## [v1.1.0-SNAPSHOT]

- Switched from clientId to client_id and maintained backward compatibility [#28835]


## [v1.0.0]

- First Release

