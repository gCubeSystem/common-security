package org.gcube.common.security.factories;

import java.util.Set;

import org.gcube.common.security.credentials.Credentials;
import org.gcube.common.security.secrets.Secret;

public interface AuthorizationProvider {

	Set<String> getContexts();
	
	Secret getSecretForContext(String context);
	
	@Deprecated
	Credentials getCredentials();

}
