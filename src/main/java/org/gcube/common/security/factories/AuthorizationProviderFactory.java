package org.gcube.common.security.factories;

import org.gcube.common.security.credentials.Credentials;

public interface AuthorizationProviderFactory<T extends AuthorizationProvider> {

	T connect(Credentials credentials);
	
}
