package org.gcube.common.security.secrets;

import java.util.Map;

import org.gcube.common.security.Owner;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public abstract class Secret {
	
	public abstract Owner getOwner();
	
	public abstract String getContext();
	
	public abstract Map<String,String> getHTTPAuthorizationHeaders();
	
	public abstract boolean isValid();
	
	public abstract boolean isExpired();
}
