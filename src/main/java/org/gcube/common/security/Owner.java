package org.gcube.common.security;

import java.util.ArrayList;
import java.util.List;

public class Owner {

	private String id;

	private List<String> roles = new ArrayList<String>();
	private List<String> globalRoles = new ArrayList<String>();
	private List<String> contextRoles = new ArrayList<String>();

	private String email;
	private String firstName;
	private String lastName;
	
	boolean externalClient;

	// "name", as the client pretty name, e.g. Catalogue for gcat, Workspace for storage-hub
	private String clientName; 

	// username of the user who requested such client
	private String contactPerson; 

	//the name of the organisation / community using such client. D4Science will be used for internal clients.
	private String contactOrganisation;		
		
	private boolean application;
	
	public Owner(String id, List<String> contextRoles, boolean external, boolean application) {
		super();
		this.id = id;
		this.contextRoles = contextRoles;
		this.roles.addAll(contextRoles);
		this.externalClient = external;
		this.application = application;
	}
	
	public Owner(String id, List<String> contextRoles, String email, String firstName, String lastName,boolean external, boolean application) {
		this(id, contextRoles, external, application);
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	public Owner(String id, List<String> contextRoles, List<String> globalRoles,  String email, String firstName, String lastName,boolean external, boolean application) {
		this(id, contextRoles, email, firstName, lastName, external, application);
		this.globalRoles = globalRoles;
		this.roles.addAll(globalRoles);
	}

	public boolean isApplication() {
		return application;
	}
	
	public String getId() {
		return id;
	}

	/**
	 * It returns all the roles of the owner independently if they are global or not
	 * @return all the role of the owner independently if they are global or not
	 */
	public List<String> getRoles() {
		return roles;
	}
	
	/**
	 * It returns the global roles of the owner
	 * @return the global roles of the owner
	 */
	public List<String> getGlobalRoles() {
		return globalRoles;
	}

	/**
	 * It returns the roles of the owner that are not global
	 * @return the roles of the owner that are not global
	 */
	public List<String> getContextRoles() {
		return contextRoles;
	}
	

	public String getEmail() {
		return email;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}

	public boolean isExternalClient() {
		return externalClient;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getContactOrganisation() {
		return contactOrganisation;
	}

	public void setContactOrganisation(String contactOrganisation) {
		this.contactOrganisation = contactOrganisation;
	}
	
}
