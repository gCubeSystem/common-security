package org.gcube.common.security.providers;

import org.gcube.common.security.secrets.Secret;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class SecretManagerProvider {

	// Thread local variable containing each thread's ID
    private static final InheritableThreadLocal<Secret> thread = new InheritableThreadLocal<Secret>() {

		@Override
		protected Secret initialValue() {
			return null;
		}

	};
    
	public static Secret get(){
		Secret secret = thread.get();
		return secret;
	}
	
	public static void set(Secret secret){
		thread.set(secret);
	}
	
	public static void reset(){
		thread.remove();
	}
	
}
